#include "ufs/file.h"
/*-
 * Copyright (c) 2011 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by UCHIYAMA Yasushi.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

__asm__("\n&FUNC    SETC 'ufs_file_deallocate'");
INT32 ufs_file_deallocate(UFSMIN *parent_dir, const char *name)
{
    UFSVDISK *vdisk = parent_dir->vdisk;
    INT32   error   = 0;
    UINT32  ino;
    UINT32  filesize;
    UFSMIN  *minode;
    char    filename[UFS_NAME_MAX + 1];

    if ((error = ufs_file_lookup_by_name(parent_dir, name, &ino)))  return error;

    minode = ufs_inode_get(vdisk, ino);
    if (!minode) return ENOENT;

    if (ufs_inode_isdir(&minode->dinode)) {
        ufs_dirent_filename(filename, name);
        /* Check parent */
        if (strcmp(filename, "..") == 0) {
            error = EINVAL;
            goto quit;
        }

        /* Check self */
        if (strcmp(filename, ".") == 0) {
            error = EINVAL;
            goto quit;
        }

        /* Check empty */
        filesize = ufs_inode_filesize(&minode->dinode);
        if (filesize > 0) {
            if (filesize != sizeof(UFSDIR) * 2) {  /*"." + ".."*/
                error = ENOTEMPTY;
                goto quit;
            }

            error = ufs_datablock_size_change(vdisk, 0, minode);
            if (error) {
                wtof("%s ufs_datablock_size_change(%08X, 0, %08X) rc = %d", __func__, vdisk, minode, error);
                goto quit;
            }
        }
        minode->dinode.nlink = 0;	/* remove this. */
    }
    else {
        /* Decrement reference count. */
        if (minode->dinode.nlink > 0) --minode->dinode.nlink;	/* regular file. */
    }

    ufs_inode_writeback(minode);

    if ((error = ufs_directory_remove_entry(vdisk, parent_dir, name))) {
        wtof("%s ufs_directory_remove_entry(%08X, %08X, \"%s\") rc = %d", __func__, vdisk, parent_dir, name, error);
        goto quit;
    }

quit:
    ufs_inode_rel(minode);

    return error;
}
