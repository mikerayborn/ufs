#include "ufs/sys.h"
#include "ufs/file.h"

__asm__("\n&FUNC    SETC 'ufs_mkdir'");
INT32   ufs_mkdir(UFS *ufs, const char *path)
{
    UFSSYS      *sys    = ufs->sys;
    UFSCWD      *cwd    = ufs->cwd;
    ACEE        *acee   = ufs->acee;
    UINT32      ino     = 0;
    UFSMIN      *minode = NULL;
    UFSVDISK    *vdisk  = NULL;
    INT32       rc = 0;
    UFSATTR     attr = {0};
    char        owner[9];
    char        group[9];
    char        fullpath[UFS_PATH_MAX+1]="";

    if (!path) return 0;

    rc = ufs_lookup(ufs, UFS_CREATE, path, &vdisk, &ino, fullpath);
    wtof("%s rc=%d, vdisk=%08X, inode=%u, fullpath=\"%s\"", __func__, rc, vdisk, ino, fullpath);
    if (!vdisk) {
        wtof("%s ufs_lookup did not return a UFSVDISK handle", __func__);
        rc = ENOENT;
        goto quit;
    }

    if (rc==0) {
        /* path name already exist */
        rc = EEXIST;
        goto quit;
    }
    if (rc!=ENOENT) goto quit;

    /* make sure we can write to file system */
    if (vdisk->disk->readonly) return EROFS;

    minode = ufs_inode_get(vdisk, ino);
    if (!minode) {
        rc = ENOENT;
        goto quit;
    }

    /* allocate new directory */
    if (acee && memcmp(acee->aceeacee, "ACEE", 4)==0) {
        /* get owner and group from security environment */
        memcpyp(owner, sizeof(owner), &acee->aceeuser[1], acee->aceeuser[0], 0);
        memcpyp(group, sizeof(group), &acee->aceegrp[1], acee->aceegrp[0], 0);
    }
    else {
        /* plug in some *safe* defaults */
        strcpy(owner, "HERC01");
        strcpy(group, "STGADMIN");
    }
    attr.owner  = owner;
    attr.group  = group;
    attr.mode   = UFS_IFDIR | ufs->create_perm;
    attr.atime  = ufs_timeval(NULL);
    attr.mtime  = attr.atime;
    attr.ctime  = attr.atime;

    /* allocate new directory */
    rc = ufs_file_allocate(minode, path, &attr, &ino);
    /* wtof("%s rc=%d, ino=%u", __func__, rc, ino); */
    if (rc) goto quit;

    /* release the inode of the parent directory */
    ufs_inode_rel(minode);

    /* get minode for this inode number */
    minode = ufs_inode_get(vdisk, ino);
    if (!minode) return ENOENT;
    /* wtodumpf(minode, sizeof(UFSMIN), "%s Child=%u", __func__, ino); */

quit:
    /* release the inode */
    if (minode) ufs_inode_rel(minode);
    return rc;
}
