#include "ufs/time.h"

/* get GMT time as double value with Unix epoch */
__asm__("\n&FUNC    SETC 'ufs_time'");
DSECS ufs_time(DSECS *dsecs)
{
    UFSTIMEV    tv  = ufs_timeval(NULL);
    DSECS       tmp;

    if (!dsecs) dsecs = &tmp;

    /* Scale the microseconds and add to our result (secs.usecs) */
    *dsecs = ((double)tv.seconds + ((double)tv.useconds / 1000000.0));

quit:
    return *dsecs;
}
