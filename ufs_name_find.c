#include "ufs/name.h"
#include "ufs/sys.h"

static UFSDEV *namrepl(UFSDEV *namedev, char name, char *newname);

__asm__("\n&FUNC    SETC 'ufs_name_find'");
UFSNAME *ufs_name_find(const char *name, char *newname, UFSSYS *sys)
{
    UFSDEV  *dev        = NULL;
    UFSDEV  *namedev    = NULL;
    UFSNAME **names     = NULL;
    INT32   lockrc      = -1;
    UINT32  len         = name ? strlen(name) : 0;
    UINT32  count;
    UINT32  n;
    char    tmpname[NM_MAXLEN];

    if (!name) return dev;
    if (!len) return dev;
    if (len >= NM_MAXLEN) return dev;

    if (!sys) return dev;
    namedev = ufs_dev_find(sys->devs, NM_NAME);
    if (!namedev) return dev;

    lockrc = lock(namedev, 0);
    if (lockrc!=0 && lockrc!=8) {
        wtof("%s lock failure namedev", __func__);
        return dev;
    }

	/* place original name in temporary buffer and null terminate */
    strcpyp(tmpname, sizeof(tmpname), name, 0);
    tmpname[sizeof(tmpname)-1] = 0;

    names = namedev->ctx1;      /* array of UFSNAME handles */
    count = arraycount(&names);

    /* repeatedly substitute the name prefix until a non-namespace */
    /* device is reached or an iteration limit is exceeded */
    for (n=0; n<count ; n++) {
        dev = namrepl(namedev, tmpname, newname);
        if (dev && dev != namdev) {
            strcpyp(tmpname, sizeof(tmpname), newname, 0);
            tmpname[sizeof(tmpname)-1] = 0;
            break;
        }
    }

    if (lockrc==0) unlock(namedev,0);
    return dev;
}

__asm__("\n&FUNC    SETC 'namrepl'");
static UFSDEV *namrepl(UFSDEV *namedev, char name, char *newname)
{
    UFSDEV  *dev    = NULL;
    UFSNAME **names = (UFSNAME **)namedev->ctx1;
    UINT32  count   = arraycount(&names);
    UINT32  n;
    UFSNAME *p;
	char	*pptr;			/* walks through a prefix	*/
	char	*optr;			/* walks through original name	*/
	INT32   plen;			/* length of a prefix string	*/
	char	*rptr;			/* walks through a replacement	*/
    char    *nptr;          /* walks through new name       */
	char	olen;			/* length of original name	*/
                            /*  including the NULL byte	*/
                            /*  *not* including NULL byte	*/
    INT32   rlen;           /* length of replacment string	*/
    INT32   remain;         /* bytes in name beyond prefix	*/

	/* search name table for first prefix that matches */
    for (n=0; n<count; n++) {
        p = names[n];
        if (!p) continue;
        if (p->dev == namedev) continue;

		optr = name;        /* start at beginning of name	*/
		pptr = p->prefix;   /* start at beginning of prefix	*/

        /* compare prefix to string and count prefix size */
        for (plen=0; *pptr != 0 ; plen++) {
            if (*pptr != *optr) break;

            pptr++;
            optr++;
		}

        if (*pptr != 0) continue;   /* prefix does not match    */

        /* Found a match - check that replacement string plus   */
        /* bytes remaining at the end of the original name will	*/
        /* fit into new name buffer.  Ignore null on replacement*/
        /* string, but keep null on remainder of name.          */
        olen = strlen(name);
        rlen = strlen(p->replace) - 1;
        remain = olen - plen;
        if ( (rlen + remain) > NM_MAXLEN) {
            return NULL;
        }

        /* place replacement string followed by remainder of */
        /* original name (and null) into the new name buffer */
        nptr = newname;
        rptr = p->replace;
        for (; rlen>0 ; rlen--) {
            *nptr++ = *rptr++;
        }
        for (; remain>0 ; remain--) {
            *nptr++ = *optr++;
        }
        dev = p->dev;
        break;
	}

    return dev;
}
