:: Create linkedit proc
::
:: Variables on input:
:: %1           C source code file path
:: %GCCCOUT%    output dataset name (optional, defaults to temp dataset)
::
:: Local stuff
@SETLOCAL ENABLEEXTENSIONS
@SET me=%~n0
@SET parent=%~dp0
::
@SET LOPTS=NCAL,LET,XREF,RENT
::
@ECHO //*
@ECHO //LKED     EXEC PGM=IEWL,
@ECHO // PARM='%LOPTS%',       
@ECHO // COND=(4,LT,ASM)   
@ECHO //SYSLIB   DD DSN=PDPCLIB.NCALIB,DISP=SHR   
@ECHO //SYSUT1   DD UNIT=SYSALLDA,SPACE=(CYL,(2,1)) 
@ECHO //SYSPRINT DD SYSOUT=*

@IF "%SYSLMOD%"=="" GOTO:LINKTEMP
@ECHO //SYSLMOD  DD DSN=%SYSLMOD%,DISP=SHR           
@GOTO INPUT

:LINKTEMP
@ECHO //SYSLMOD  DD DSN=^&^&SYSLMOD(TEMPNAME),DISP=(,PASS),UNIT=SYSALLDA,   
@ECHO //            DCB=(LRECL=0,BLKSIZE=15040,RECFM=U),    
@ECHO //            SPACE=(15040,(500,500,5))                   
@ECHO //*

:INPUT
@IF "%ASMOUT%"=="" GOTO:DUMMY
@ECHO //SYSLIN   DD DDNAME=SYSIN                    
@ECHO //SYSIN    DD DSN=%ASMOUT%,DISP=SHR
@GOTO DONE 

:DUMMY                          
@ECHO //SYSLIN   DD DDNAME=SYSIN                    
@ECHO //SYSIN    DD DUMMY                           

:DONE
