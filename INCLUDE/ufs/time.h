#ifndef UFS_TIME_H
#define UFS_TIME_H

#include <time.h>               /* standard library time    */
#include <socket.h>             /* has struct timeval, go figure! */
#include "ufs/types.h"          /* our atomic types         */

#ifndef TYPE_UFSTIMEV
#define TYPE_UFSTIMEV
typedef struct ufs_timeval
{
    UINT32  seconds;
    UINT32  useconds;
} UFSTIMEV;
#endif

/* get GMT time as double (secs.usecs) value with Unix epoch (dsecs can be NULL) */
DSECS ufs_time(DSECS *dsecs)                                        asm("UFSDSECS");
DSECS ufs_dsecs(DSECS *dsecs)                                       asm("UFSDSECS");

/* get GMT time as timeval value with Unix epoch (tv can be NULL)*/
UFSTIMEV ufs_timeval(UFSTIMEV *tv)                                  asm("UFSDSTV");

#endif /* UFS_TIME_H */
